package com.bos.bean;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

public class PageBean {
	//当前页
	private int currentPage;
	//查询多少个  pageSize
	private int pageSize;
	private DetachedCriteria criteria;
	
	//结果集
	private List rows;
	
	//总记录数
	private int total;
	

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List getRows() {
		return rows;
	}

	public void setRows(List rows) {
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public DetachedCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(DetachedCriteria criteria) {
		this.criteria = criteria;
	}
	
	
	
	
	
}
