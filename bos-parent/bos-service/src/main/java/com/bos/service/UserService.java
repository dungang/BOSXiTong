package com.bos.service;

import com.bos.bean.PageBean;
import com.bos.bean.User;

public interface UserService {
	
	User login(String userName,String password);

	void update(User user);

	void save(User model, String[] roleId);

	void pageQuery(PageBean pageBean);
}
