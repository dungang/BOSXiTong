package com.bos.service;

import java.util.List;

import com.bos.bean.PageBean;
import com.bos.bean.Role;

public interface RoleService {

	void save(Role model, String functionIds);

	void pageQuery(PageBean pageBean);

	List<Role> findAll();

}
