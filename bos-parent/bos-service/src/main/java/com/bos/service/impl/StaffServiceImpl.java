package com.bos.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bos.bean.PageBean;
import com.bos.bean.Staff;
import com.bos.dao.StaffDao;
import com.bos.service.StaffService;

@Service
@Transactional
public class StaffServiceImpl implements StaffService{

	@Autowired
	private StaffDao dao;
	
	public void save(Staff model) {
		dao.insert(model);
	}

	public void pageQuery(PageBean pageBean) {
		dao.pageQuery(pageBean);
	}

	@Override
	public void delbatch(String ids) {
		//分割ids   分割成id
		String[] idArr = ids.split(",");
		//更新方法     需要 附带条件的更新方法    update form bc_staff set deltag = "1" where id = id
		//批量的调用dao的按照条件查询的方法
		for (String id : idArr) {
			dao.updateByQueryName("staff.delete", id);
		}
	}

	@Override
	public void update(Staff model) {
		dao.update(model);
	}

	@Override
	public Staff findById(String id) {
		return dao.findById(id);
	}
	
	//查询未被删除的员工
	public List<Staff> findNoDel() {
		//创建一个离线查询对象
		DetachedCriteria criteria = DetachedCriteria.forClass(Staff.class);
		//加条件  
		criteria.add(Restrictions.eq("deltag", "0"));
		//查询员工
		return dao.findByCriteria(criteria);
	}
}
