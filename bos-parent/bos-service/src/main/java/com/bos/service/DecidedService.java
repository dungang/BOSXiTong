package com.bos.service;

import com.bos.bean.DecidedZone;
import com.bos.bean.PageBean;

public interface DecidedService {
	void save(DecidedZone model, String[] subareaid);
	void pageQuery(PageBean pageBean);
	DecidedZone findById(String id);
}
