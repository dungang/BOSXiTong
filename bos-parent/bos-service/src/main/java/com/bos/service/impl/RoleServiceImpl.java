package com.bos.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bos.bean.Function;
import com.bos.bean.PageBean;
import com.bos.bean.Role;
import com.bos.dao.RoleDao;
import com.bos.service.RoleService;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	public void save(Role model, String functionIds) {
		roleDao.insert(model);
		//给角色绑定权限
		//利用id生成权限对象    给role 设置权限
		String[] functionArray = functionIds.split(",");
		for (String functionId : functionArray) {
			//通过functionid 创建 function对象   
			//1. 通过id查询数据库查询到function 的持久化对象  给角色设置权限
			//2. 通过id直接创建function对象  直接设置   托管对象
			Function function = new Function(functionId);
			//给角色绑定权限
			model.getFunctions().add(function);
		}
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		// TODO Auto-generated method stub
		roleDao.pageQuery(pageBean);
	}

	public List<Role> findAll() {
		return roleDao.getAll();
	}
	
}
