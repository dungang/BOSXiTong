package com.bos.service;

import java.util.List;

import com.bos.bean.PageBean;
import com.bos.bean.Staff;

public interface StaffService {

	void save(Staff model);

	void pageQuery(PageBean pageBean);

	void delbatch(String ids);

	void update(Staff model);

	Staff findById(String id);

	List<Staff> findNoDel();
}
