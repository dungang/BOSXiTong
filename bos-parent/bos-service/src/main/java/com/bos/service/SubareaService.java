package com.bos.service;

import java.util.List;

import com.bos.bean.PageBean;
import com.bos.bean.Subarea;

public interface SubareaService {

	void save(Subarea m);

	void pageQuery(PageBean pageBean);

	List<Subarea> findNoRelevance();
}
