package com.bos.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bos.bean.PageBean;
import com.bos.bean.Region;
import com.bos.dao.RegionDao;
import com.bos.dao.base.BaseDaoImpl;

@Repository
public class RegionDaoImpl extends BaseDaoImpl<Region> implements RegionDao {

	@Override
	public List<Region> findByQ(String q) {
		// TODO Auto-generated method stub
		String hql = "FROM Region r where r.province LIKE ? OR r.city LIKE ? OR r.district LIKE ? "
				+ "OR r.shortcode LIKE ? OR r.citycode LIKE ?";
		q = "%" + q + "%";
		List<Region> regions = (List<Region>) getHibernateTemplate().find(hql, q,q,q,q,q);
		return regions;
	}
}
