package com.bos.dao;

import com.bos.bean.DecidedZone;
import com.bos.dao.base.BaseDao;

public interface DecidedDao extends BaseDao<DecidedZone> {
}
