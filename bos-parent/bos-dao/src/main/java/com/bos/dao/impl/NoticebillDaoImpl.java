package com.bos.dao.impl;

import org.springframework.stereotype.Repository;

import com.bos.bean.Noticebill;
import com.bos.dao.NoticebillDao;
import com.bos.dao.base.BaseDaoImpl;

@Repository
public class NoticebillDaoImpl extends BaseDaoImpl<Noticebill> implements NoticebillDao {
}
