package com.bos.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.bos.bean.User;
import com.bos.dao.UserDao;
import com.bos.dao.base.BaseDaoImpl;

@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao{

	public User findUserByUserNameAndPwd(String userName, String password) {
		String hql = "FROM User where username = ? and password = ?";
		List users = getHibernateTemplate().find(hql, userName, password);
		if (users != null && users.size() > 0) {
			return (User)users.get(0);
		}
		return null;
	}

	public User findUserByUserName(String username) {
		String hql = "FROM User where username = ?";
		List users = getHibernateTemplate().find(hql, username);
		if (users != null && users.size() > 0) {
			return (User)users.get(0);
		}
		return null;
	}

}
