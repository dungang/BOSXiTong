package com.bos.dao.impl;

import org.springframework.stereotype.Repository;

import com.bos.bean.Role;
import com.bos.dao.RoleDao;
import com.bos.dao.base.BaseDaoImpl;

@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao {

	
}
