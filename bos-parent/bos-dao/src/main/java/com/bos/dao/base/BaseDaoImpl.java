package com.bos.dao.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Component;

import com.bos.bean.PageBean;

public class BaseDaoImpl<T> extends HibernateDaoSupport implements BaseDao<T> {

	private Class entityClass;
	
	// BaseDao dao = new BaseDaoImpl<User>()
	//就是给EntityClass赋值   就是获取  T  通过反射机制   对象创建了 构造方法
	public BaseDaoImpl() {
		//获取T    
		//获取到父类的类型  上的T
		ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
		//获取到类上边定义的所有参数  的数组
		Type[] actualTypeArguments = genericSuperclass.getActualTypeArguments();
		entityClass = (Class) actualTypeArguments[0];
	}
	
	//解决hibernatetemplate是null的问题  使用注解来解决null的问题
	@Resource
	public void setSessionFactory2(SessionFactory sessionFactory)
	{
		super.setSessionFactory(sessionFactory);
	}
	

	public void insert(T bean) {
		getHibernateTemplate().save(bean);
	}

	@Override
	public void delete(T bean) {
		getHibernateTemplate().delete(bean);
	}

	@Override
	public void update(T bean) {
		getHibernateTemplate().update(bean);
	}

	public T findById(Serializable id) {
		//只有程序跑起来的时候才知道
		return (T) getHibernateTemplate().get(entityClass, id);
	}

	@Override
	public List<T> getAll() {  
		String hql = "FROM " + entityClass.getSimpleName();
		return (List<T>) getHibernateTemplate().find(hql, null);
	}

	public void pageQuery(PageBean pageBean) {
		DetachedCriteria criteria = pageBean.getCriteria();
		//获取当前页
		int currentPage = pageBean.getCurrentPage();
		int pageSize = pageBean.getPageSize();
		//开始查询的位置
		int firstResult =  (currentPage-1)*pageSize;
		// 查询 总记录数  的条件
		criteria.setProjection(Projections.rowCount()); // SELECT count(*) from Staff
		
		//执行查询
		List<Long> countList = (List<Long>) getHibernateTemplate().findByCriteria(criteria);
		//获取总记录数
		int total = countList.get(0).intValue();
		//设置无查询条件
		criteria.setProjection(null); //FROM Staff
		//添加一个语句
		criteria.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		//分页的结果集
		List rows = getHibernateTemplate().findByCriteria(criteria, firstResult, pageSize);
		//放入到pageBean
		pageBean.setRows(rows);
		pageBean.setTotal(total);
	}

	public void updateByQueryName(String queryName, Object... parm) {
		Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		Query query = session.getNamedQuery(queryName);
		//批量的更新
		int i = 0;
		for (Object object : parm) {
			query.setParameter(i++, object);
		}
		query.executeUpdate();
	}

	/**
	 * 保存或者更新
	 */
	public void saveOrUpdate(T bean) {
		getHibernateTemplate().saveOrUpdate(bean);
	}

	public List findByCriteria(DetachedCriteria criteria) {
		return getHibernateTemplate().findByCriteria(criteria);
	}
}
