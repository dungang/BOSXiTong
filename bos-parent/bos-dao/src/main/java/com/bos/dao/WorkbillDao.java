package com.bos.dao;

import com.bos.bean.Workbill;
import com.bos.dao.base.BaseDao;

public interface WorkbillDao extends BaseDao<Workbill> {
}
