<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>
</head>
<body>
	<!-- 手动创建datagrid数据  了解 -->
	<table class="easyui-datagrid">
		<thead>
			<tr>
				<th field = "id">编号</th>
				<th field = "name">姓名</th>
				<th field = "age">年龄</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>001</td>
				<td>张三</td>
				<td>120</td>
			</tr>		
			<tr>
				<td>002</td>
				<td>张2 </td>
				<td>120</td>
			</tr>		
			<tr>
				<td>001</td>
				<td>张1 </td>
				<td>120</td>
			</tr>		
		</tbody>
	</table>
	<hr>
	<br>
	<!-- 第二种方式 使用JSON数据来创建数据表格 
		JSON数据的一个特性   无序的   了解
	-->
	<table class="easyui-datagrid" url ="${pageContext.request.contextPath}/json/datagrid.json" >
		<thead>
			<tr>
				<th field = "id">编号</th>
				<th field = "name">姓名</th>
				<th field = "age">年龄</th>
			</tr>
		</thead>
	</table>
	<hr>
	<br>
		
	<!-- 重点掌握 -->
	<table id = "dataGridTb"></table>
	<script type="text/javascript">
		//文档加载好之后的事件   
		$(function(){
			$("#dataGridTb").datagrid(
				{
					url:'${pageContext.request.contextPath}/json/datagrid.json',
					columns:[[
					          //将这一列变成复选框
					          {field:'id',title:"编号",width:100,checkbox:true},
					          {field:'name',title:"姓名",width:200},
					          {field:'age',title:"年龄",width:200}
					          ]],
					//工具栏
					toolbar:[{
						text:"添加",
						iconCls:'icon-add',
						handler:function(){
							alert("add");
						}
					}],
					//分页条
					pagination:true,
				}
			);
		})	
	
	
	</script>

</body>
</html>