<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/locale/easyui-lang-zh_CN.js"></script>

</head>
<body>

	<table id = "grid_tb" class = "easyui-datagrid"></table>	
	
	<script type="text/javascript">
		$(function(){
			//
			$("#grid_tb").datagrid({
				columns:[[
				          {title:"编号",field:"id",checkbox:true},
				          {title:"姓名",field:"name"},
				          {title:"年龄",field:"age"}
				          ]],
				url:'${pageContext.request.contextPath}/json/data.json',
				//显示行号
				rownumbers:true,
				singleselect:true,
				//定义工具栏
				toolbar:[
				         {
				        	 text:'添加',
				        	 iconCls:'icon-add',
				        	 handler:function(){
				        		 alert("add");
				        	 }
				         }],
				        //显示分页条 
				pagination:true	
				
			});
			
			
		});
	
	</script>
	
</body>
</html>