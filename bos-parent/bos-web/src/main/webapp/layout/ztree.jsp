<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/ztree/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/ztree/jquery.ztree.all-3.5.js"></script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north'" style="height:100px " title="北方" iconCls="icon-ok">北方</div>
	<div data-options="region:'south', title:'南方'" style="height: 120px"  >南部</div>
	<div data-options="region:'east'" style="width:80px " title="东部" >东部</div>
	<div class="easyui-accordion" data-options="region:'west'" style="width: 180px" title="西部" >
		<!-- 折叠面板 -->
		<div title = "first" iconCls="icon-back">
			<a class="easyui-linkbutton" href="#" onclick="addTabs('baidu','https://www.baidu.com')" >百度</a>
			<a class="easyui-linkbutton" href="#" onclick="addTabs('mayun','http://git.oschina.net')" >码云</a>
			<a class="easyui-linkbutton" href="#" onclick="addTabs('github','https://github.com')" >github</a>
		</div>
		<div title="second">
		<ul id = "tu" class = "ztree"></ul>
			<!-- 种树 -->
		</div>
		<div title="third">第三个面板</div>
	</div>
	<div id="tt" region="center" title="中部" class="easyui-tabs">
	</div>
	<script type="text/javascript">
		/*
		1.setting
		2.节点  Znodes
		3.初始化
		*/
		/* var setting = {};
		var zNodes = [{'name':'节点1',children:[{'name':'子节点1',children:[{'name':'孙子节点1'},{'name':'孙女节点2'}]},{'name':'子节点2'}]},
		              {'name':'节点2'},
		              {'name':'节点3'},
		              {'name':'节点4'}];
		$.fn.zTree.init($("#tu"), setting, zNodes); */
		/* var setting1 = {
				data:{
				simpleData:{enable:true}
				}
		}
		var zNodes1 = [
		              {id:1,pId:0,'name':'节点1'},
		              {id:11,pId:1,'name':'子节点1'},
		              {id:12,pId:1,'name':'子节点2'},
		              {id:13,pId:1,'name':'子节点3'},
		              {id:2,pId:0,'name':'节点1'},
		              {id:21,pId:2,'name':'2子节点1'},
		              {id:22,pId:2,'name':'2子节点2'}
		]
		$.fn.zTree.init($("#tu"), setting1, zNodes1); */
		//耦合度还是很高   降低耦合度  加载文件
		var setting1 = {
				data:{
				simpleData:{enable:true}
				},callback: {
					onClick: function(event,treeId,treeNode)
					{
						//如果 treeNode.page属性为undefined的时候就啥也不干
						if(treeNode.page != undefined)
						{
							var title = treeNode.name;
							if($('#tt').tabs('exists',title))
							{
								//xuanzhong 
								$('#tt').tabs('select',title);
								return;
							}
							//每一个子节点的点击事件
							var content = '<iframe scrolling="auto" frameborder="0"  src="${pageContext.request.contextPath}/'+treeNode.page+'" style="width:100%;height:100%;"></iframe>';
							//添加一个标签页
							$("#tt").tabs('add',{
								title:treeNode.name,
								content:content,
								closable:true
							});
						}
						
					}
				}
		}
		//加载文件   网络请求     post Ajax
		$.post('${pageContext.request.contextPath}/json/menu.json',{},function(data,status)
				{
					$.fn.zTree.init($("#tu"), setting1, data);
				},'json');
	</script>
</body>
</html>