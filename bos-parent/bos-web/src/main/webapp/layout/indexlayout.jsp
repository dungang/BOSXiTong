<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/ztree/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/ztree/jquery.ztree.all-3.5.js"></script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north'" style="height:100px " title="北方" iconCls="icon-ok">北方</div>
	<div data-options="region:'south', title:'南方'" style="height: 120px"  >南部</div>
	<div data-options="region:'east'" style="width:80px " title="东部" >东部</div>
	<div class="easyui-accordion" data-options="region:'west'" style="width: 100px" title="西部" >
		<!-- 折叠面板 -->
		<div title = "first" iconCls="icon-back">
			<ul id = "ztree1" class="ztree"></ul>
		</div>
		<div title="second" >
			<ul id ="ztree2" class="ztree"></ul>	
		</div>
		<div title="third">第三个面板</div>
	</div>
	<div region="center" title="中部" class="easyui-tabs">
		<div title="第一个" closable="true" ></div>
		<div title="第2个" closable="true" >
		 <iframe scrolling="auto" frameborder="0"  src="https://www.baidu.com" style="width:100%;height:100%;"></iframe>
		</div>
		<div title="第3个" closable="true"></div>
	</div>
	
	<script type="text/javascript">
		$(function(){
			var setting = {};
			var zNodes = [{'name':'节点1',children:[{'name':'子节点1'},{'name':'子节点2'}]},{'name':'节点2'},{'name':'节点3'}];
			$.fn.zTree.init($('#ztree1'),setting,zNodes);
			
			var setting1 = {
					data:{simpleData:{enable:true}}
			};
			var zNodes1 = [{'id':'1','pId':'0','name':'节点1'}
						,{'id':'2','pId':'1','name':'子节点1'},
						{'id':'3','pId':'2','name':'子节点2'}
			]
			$.fn.zTree.init($('#ztree1'),setting1,zNodes1);
		});
		
		
	</script>
	
</body>
</html>