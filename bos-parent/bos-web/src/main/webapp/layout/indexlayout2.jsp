<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north'" style="height:100px " title="北方" iconCls="icon-ok">北方</div>
	<div data-options="region:'south', title:'南方'" style="height: 120px"  >南部</div>
	<div data-options="region:'east'" style="width:80px " title="东部" >东部</div>
	<div class="easyui-accordion" data-options="region:'west'" style="width: 100px" title="西部" >
		<!-- 折叠面板 -->
		<div title = "first" iconCls="icon-back">
			<a class="easyui-linkbutton" href="#" onclick="addTabs('baidu','https://www.baidu.com')" >百度</a>
			
			<a class="easyui-linkbutton" href="#" onclick="addTabs('mayun','http://git.oschina.net')" >码云</a>

			<a class="easyui-linkbutton" href="#" onclick="addTabs('github','https://github.com')" >github</a>
		</div>
		<div title="second">第二个面板</div>
		<div title="third">第三个面板</div>
	</div>
	<div id="tt" region="center" title="中部" class="easyui-tabs">
		<!-- 动态添加 -->
	</div>
	
	<script type="text/javascript">
		function addTabs(title,url)
		{
			//判断  是否已经创建出来了   如果创建了那么就选中它   否则就创建它
			if($('#tt').tabs('exists',title))
			{
				$('#tt').tabs('select',title);
			}else{
				var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
				//添加一个标签页
				$("#tt").tabs('add',{
					title:title,
					content:content,
					closable:true
				});
			}
		}
	
	</script>
	
	
</body>
</html>