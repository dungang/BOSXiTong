<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.ocupload-1.1.2.js"></script>
<title>Insert title here</title>
</head>
<body>
	<input id="uploadbtn" type="button" value="提交按钮">
	<script type="text/javascript">
		$("#uploadbtn").upload({
			name : "regionFile",
			action:"${pageContext.request.contextPath}/regionAction_importXls.action",
			onSelect:function(){
				//关闭自动提交
				this.autoSubmit = false;
				//excel正则
				var reg =  /^.*\.(?:xls|xl|xla|xlt|xlm|xlc|xlw)$/;
				//获取fileName 
				var fileName = this.filename();
				if(reg.test(fileName))
				{
					this.submit();
				}else{
					alert("只能上传Excel表格");
				}
			},
			onComplete:function(){
				alert("提交成功");
			}
		});
	</script>

</body>
</html>