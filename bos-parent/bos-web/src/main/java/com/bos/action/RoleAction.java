package com.bos.action;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bos.bean.Role;
import com.bos.service.RoleService;

@Controller
@Scope("prototype")
public class RoleAction extends BaseAction<Role> {

	@Autowired
	private RoleService roleService;
	
	private String functionIds;
	
	public String add(){
		roleService.save(model,functionIds);
		return LIST;
	}

	public void setFunctionIds(String functionIds) {
		this.functionIds = functionIds;
	}
	
	public String pageQuery() throws IOException{
		roleService.pageQuery(pageBean);
		objToJson(pageBean, new String[]{"currentPage", "pageSize","criteria","users","functions"});
		return NONE;
	}
	
	public String findAll() throws IOException{
		List<Role> roles = roleService.findAll();
		listToJSON(roles, new String[]{"users","functions"});	
		return NONE;
	}
	
}
