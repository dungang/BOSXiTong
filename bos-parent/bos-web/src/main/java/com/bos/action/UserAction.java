package com.bos.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.components.Password;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bos.bean.User;
import com.bos.service.UserService;
import com.bos.utils.MD5Utils;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@Controller
@Scope("prototype")     //多实例
public class UserAction extends BaseAction<User>  {

	@Autowired
	UserService userService;
	
	private String checkcode;
	
	private String[] roleId;
	
	
	//Shiro认证的登录
	public String login(){
		// 用户名和密码是否为空  是否大于6位数
		// 验证码
		//获取session中的验证码
		//1.获取session  2. 获取验证码
		//String keyCode = ServletActionContext.getRequest().getSession().getAttribute("key").toString();
		String actrualYzm = session.get("key").toString();
		//验证验证码
		if(checkcode != null && !checkcode.isEmpty() && actrualYzm.equals(checkcode) ){
			//Shiro认证
			Subject subject = SecurityUtils.getSubject();
			AuthenticationToken token = new UsernamePasswordToken(model.getUsername(),MD5Utils.md5(model.getPassword()));
			try {
				//如果认证失败就会抛异常
				subject.login(token);
				//如果认证成功了就可以获取到User对象
				User user = (User) subject.getPrincipal();
				session.put("userInfo",user);
			} catch (Exception e) {
				e.printStackTrace();
				return LOGIN;
			}
			return HOME;
		}else{
			//提示   验证码错误
			this.addActionError("验证码错误");
			return LOGIN;
		}
	}
	
	public String add(){
		userService.save(model,roleId);
		return LIST;
	}
	
	public String pageQuery() throws IOException{
		userService.pageQuery(pageBean);
		objToJson(pageBean, new String[]{"currentPage", "pageSize", "criteria","noticebills","roles"});
		return NONE;
	}
	
	
	public String login_1(){
		// 用户名和密码是否为空  是否大于6位数
		// 验证码
		//获取session中的验证码
		//1.获取session  2. 获取验证码
		//String keyCode = ServletActionContext.getRequest().getSession().getAttribute("key").toString();
		String actrualYzm = session.get("key").toString();
		//验证验证码
		if(checkcode != null && !checkcode.isEmpty() && actrualYzm.equals(checkcode) ){
			User user = userService.login(model.getUsername(), model.getPassword());
			if(user != null)
			{
				//登录成功
				//将用户信息放到Session中保存
				session.put("userInfo", user);
				return HOME;
			}else{
				//登录失败
				this.addActionError("账号或密码错误");
				return LOGIN;
			}
		}else{
			//提示   验证码错误
			this.addActionError("验证码错误");
			return LOGIN;
		}
	}
	
	public String editPwd(){
		String password = model.getPassword();  //获取网页提交的密码
		// 修改密码  
		//从session中拿到 我们的user对象
		User user = (User) session.get("userInfo");
		user.setPassword(password);
		userService.update(user);
		//向浏览器返回一个数据   登录成功的 标识
		//获取到response  
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("text/html;charset=utf-8");
		try {
			PrintWriter writer = response.getWriter();
			writer.print("ok");
			response.setStatus(200);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 清空session跳转到登录页面
		session.clear();
		return NONE;
	}
	
	
	public String logout()
	{
		//清除session
		//session.clear();
		ServletActionContext.getRequest().getSession().invalidate();
		return LOGIN;
	}


	public String getCheckcode() {
		return checkcode;
	}

	public void setCheckcode(String checkcode) {
		this.checkcode = checkcode;
	}

	public String[] getRoleId() {
		return roleId;
	}

	public void setRoleId(String[] roleId) {
		this.roleId = roleId;
	}
}
