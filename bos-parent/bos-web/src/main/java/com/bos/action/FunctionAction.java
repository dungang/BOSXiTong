package com.bos.action;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bos.bean.Function;
import com.bos.service.FunctionService;

@Controller
@Scope("prototype")
public class FunctionAction extends BaseAction<Function> {

	@Autowired
	private FunctionService functionService;
	
	public String pageQuery() throws IOException{
		// page属性被 模型驱动拦截了  所以我们需要手动设置page
		pageBean.setCurrentPage(Integer.parseInt(model.getPage()));
		functionService.pageQuery(pageBean);
		
		objToJson(pageBean, new String[] { "currentPage", "pageSize", "criteria","parentFunction","children","roles" });
		return NONE;
	}
	
	
	public String findAll() throws IOException{
		List<Function> list = functionService.findAll();
		listToJSON(list, new String[]{"parentFunction","roles"});
		return NONE;
	}
	
	
}
